# [CEPH](https://ceph.com) rebuild for RHEL/CentOS/SL(C) 6
Since ceph infernalis (9.X) rhel6 builds are no more provided upstream. 
This project offers rhel6 rebuilds of upstream releases.

## Available releases:

- [jewel (10.X)](https://gitlab.cern.ch/linuxsupport/ceph-for-el6/tree/jewel)
- [luminous (12.X)](https://gitlab.cern.ch/linuxsupport/ceph-for-el6/tree/luminous) 


## How to rebuild on el6
On Red Hat Enterprise Linux / CentOS / Scientific Linux (CERN) 6 aditional software repositories
are needed in order to rebuild ceph packages:

- [EPEL](https://fedoraproject.org/wiki/EPEL)
- [Software Collections](https://www.softwarecollections.org/en/)

### Repositories installation on SLC6
EPEL repository is pre-installed in standard setup.
To install Software Collections please see: [instructions](https://cern.ch/linux/scientific6/docs/softwarecollections.shtml)

--
Jarek Polok
